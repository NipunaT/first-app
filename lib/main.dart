import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Center(
            child: Text('IMPERIAL'),
          ),
        ),
        body:  Center(
          child: ColoredBox(
            child:Text('this is a test') ,
            color: Colors.amber,
          ),

        ) ,
        floatingActionButton: FloatingActionButton(),
      ),
    ),
  );
}
